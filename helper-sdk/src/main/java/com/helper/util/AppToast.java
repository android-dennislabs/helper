package com.helper.util;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.helper.R;

public class AppToast {
    public static void success(Context context, String message) {
        if(context instanceof Activity){
            Activity activity = (Activity) context;
            View layout = activity.getLayoutInflater().inflate(R.layout.lib_toast_success, (ViewGroup) activity.findViewById(R.id.lib_toast_success_layout));
            TextView tvTitle = layout.findViewById(R.id.tv_title);

//            Config config = new Config.Builder()
//                    .autoplay(true)
//                    .speed(2f)
//                    .loop(true)
//                    .source(new DotLottieSource.Asset("lwkdrhb0.lottie"))
//                    .playMode(Mode.FORWARD)
//                    .build();
//
//            DotLottieAnimation lottieView = layout.findViewById(R.id.lottie_view);
//            lottieView.load(config);

            ImageView imageView = layout.findViewById(R.id.lib_helper_iv_gif);
            Glide.with(context.getApplicationContext())
                    .asGif()
                    .load(R.raw.gif_tick_success)
                    .into(imageView);

            tvTitle.setText(message);
            Toast toast = new Toast(activity);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout);//setting the view of custom toast layout
            toast.show();

//            DotLottieAnimation lottieView = toast.getView().findViewById(R.id.lottie_view);
//            lottieView.load(config);
//            lottieView.play();

        }else {
            BaseUtil.showToastCentre(context, message);
        }
    }

    public static void failure(Context context, String message) {
        if(context instanceof Activity) {
            Activity activity = (Activity) context;
            View layout = activity.getLayoutInflater().inflate(R.layout.lib_toast_success, (ViewGroup) activity.findViewById(R.id.lib_toast_success_layout));

            TextView tvTitle = layout.findViewById(R.id.tv_title);
            tvTitle.setText(message);

            ImageView imageView = layout.findViewById(R.id.lib_helper_iv_gif);
            // Set size in dp
            int desiredSizeInDp = 70;
            float density = context.getResources().getDisplayMetrics().density;
            int desiredSize = Math.round(desiredSizeInDp * density);

            // Set the desired margin in dp
            int desiredMarginInDp = 20;
            int desiredMarginInPixels = Math.round(desiredMarginInDp * density);

            // Create new margin layout parameters for the ImageView
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) imageView.getLayoutParams();
            layoutParams.width = desiredSize;
            layoutParams.height = desiredSize;
            layoutParams.setMargins(desiredMarginInPixels, desiredMarginInPixels, desiredMarginInPixels, desiredMarginInPixels);


            Glide.with(context.getApplicationContext())
                    .asGif()
                    .load(R.raw.gif_failure)
                    .into(imageView);

            Toast toast = new Toast(activity);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout);//setting the view of custom toast layout
            toast.show();
        }else {
            BaseUtil.showToastCentre(context, message);
        }
    }

}
